#  CommercioKYC (ComKYC) Project Summary

For more information, please contact: 

Enrico Talin
Contacts: enrico.talin@commerc.io , @enrico on this GitLab.


## Introduction

We want to create a KYC protocol where any Company can  onboard customers  with high confidence about their true Identity and Customers  will instantly sign-up   effortlessly.



### About Commercio ecosystem

* Commercio.network is a public-permissioned blockchain that provides a set of protocols and SDKs to manage eID, eSignature and eDelivery. 
* Commerc.io srl is the startup company that developed the Open-Source eIDAS Compliant Document Blokchain software and project.
* CommercioConsortium.org is the organization that launched the project and is currently managing the Blockchain node infrastructure:

In the next five years as a shared effort as a network we are aiming to reach the following objectives:

- 100 validator Nodes who will provide the validation services for the Blockchain ( mainly TSP and QTSP EU companies )
- 1000 Distributors who will promote commercio.network memberships and Credits to Developers and end users (members)
- 100.000 Developers who will create applications with our freeSDK using our Open Source eID, eSignature and eDelivery protocols
- 100.000.000 Network Members who will sign-up and sign-in on developers' applications


### CommercioKYC

We want to create a KYC protocol that will empower both Companies and individuals:

A Company will onboard customers with high confidence about their true Identity and simplify the internal KYC process and potentially limit privacy data Liabilities
A Customer will instantly sign-up and sign-in effortlessly to a service, without losing control of their role identity holders, without being forced to request permission of an intermediary or centralised authority and gives control over how their personal data is shared and used.

CommercioKYC is a  protocol that enables to instantly Issue a KYC VC (Verifiable Credential) based on data accessed through PSD2 Bank Payment Service  Directives
CommercioKYC will:


Improve customer experience Automating the KYC process by obtaining information straight from end-customers’ banks accounts  instead of asking them to manually input form fields or send in physical documents.

Reduce user drop-out By making it easier and quicker for end-customers to sign up and start using a company service, we increase the chances of them successfully completing the process.

instantly issue a pre-verified Credential Since the data comes directly from the users’ banks, it can be trusted with a minimum, sufficient and satisfactory level of assurance. There is no need to wait days to have the information to be manually verified if a company decides that a Bank can be trusted.

Why Banks? Simply put banks have the most rigid, rigorous, and stringent KYC requirements of the world, also they have the biggest penalties not to enforce them. So in our opinion banks make the best candidates for solving our problem. How? We want to piggyback on banks' strict requirements to create a fully interoperable self-sovereign identity KYC VC (Verifiable Credentials)  solution.

##  One sentence Summary

We want to create a KYC protocol where any Company can  onboard customers  with high confidence about their true Identity and Customers  will instantly sign-up effortlessly.

### Business Problem

* Any Company that onboards  new customers need to ask them  **A LOT** of  information. 
* Some companies are required to check if the information provided by the customers is true.
* This process is known as  KYC  Know Your Customer, and it means taking steps to identify customers and checking they are who they say they are. 
* KYC is often associated  with the Anti Money Laundering (AML) regulations to limit the potential risks of  interacting with Criminal Actors during your business relationships.
* KYC is mandatory in many  regulated industry (Crypto, Insurance, Telco, Hospitality, etc) EU legislators are constantly broadening the KYC and AML reach. 
* We believe in the future will be eventually  mandatory for all Companies in the EU... 
* KYC is a  **BIG PAIN** for  Customers... Companies… Regulators… and it is repeated over and over endlessly...  
* We need to find a better solution



### Technical Solution

CommercioKYC is a protocol that Issue a KYC VC Verifiable Credentials based on PSD2 Bank Payment Service Directives 


1. The end-user requests an identity verifiable credential to CommercioKYC, using a supported payment system as an identity proof method.
1. The end-user performs bank access/payment via the agreed payment system and CommercioKYC verifies the end-user personal data on the base of the completed transaction.
1. In case of successful verification, CommercioKYC issues an identity credential for the user. The end-user accepts it and, from that moment on, shall be able to present this credential to other verifiers.


All the steps will be performed on:

- A frontend  Commercio.network BIP39 wallet with CommericioID protocols that support DIDs verifier and issuer functionality for holders
- A backend Commercio.network API server that supports holder/presentation and issuer functionality with Pairwise private DIDs to protect citizens against data hoarding. 

W3C Decentralized Identifiers (DIDs) and the [VCA](https://gitlab.grnet.gr/essif-lab/infrastructure/nym-srl/vca_project_summary) By Nym srl


## Integration with the eSSIF-Lab Functional Architecture and Community

CommercioKYC is collaborating with the following eSSIF-lab Functional Architecture  projects:

- [VCA](https://gitlab.grnet.gr/essif-lab/infrastructure/nym-srl/vca_project_summary)

CommercioKYC plans to collaborate with the following eSSIF-lab Commercial sub-projects:


### Universal DID SaaS  ( markus@danubetech.com)

* Universal DID SaaS will provide guidance support for the DID method (did:com) used by  CommericioKYC, by creating a "driver" for the Universal Resolver
* CommercioKYC will provide a real world use case and potential customers for  Universal DID SaaS for providing interoperability with other blockchains

### UBS  ( irene@jolocom.com )
* UBS would provide a universal back-up service for Data storage to the customers of CommericioKYC 
* CommercioKYC would provide UBS a real world use case and potential customers 
* We will host a joint workshop between interested projects to see how best to create a UBS that works for everyone
* We will also use synergies of developer experience (regarding for example VCs, DID.Comm, …) 

### IRIS-DCC (nick@resonate.is)
* IRIS-DCC  will provide  a specific set of earnable badges verifiable credentials to create a  more flexible and secure workflow to CommericioKYC
* CommercioKYC will provide DID Wallet and DID auth  to IRIS-DCC plus the SEPA payment KYC Credentials workflow

### GAYA  (egidio.casati@nymlab.it)
* GAYA will provide CommercioKYC additional Video Conferencing Capabilities and Public Notary Support for extended Verification of the Company Owner and legal representative on a need basis.
* CommercioKYC will provide GAYA the DID Wallet and DIDauth infrastructure 	 plus the SEPA payment KYC Credentials workflow 

### SSI4DTM (projects@joinyourbit.com)
* CommercioKYC will provide SSI4DTM the DID Wallet and DIDauth infrastructure  plus the SEPA payment KYC Credentials workflow
* SSI4DTM will provide CommercioKYC the “SSI eIDAS bridge” with Advanced Electronic Signature for physical and legal person (certificate and eSeal)

### DAC  (petri.seljavaara@wellbeingcart.com)
* CommercioKYC will provide DAC the DID Wallet and DIDauth infrastructure plus the SEPA payment KYC Credentials workflow 
* DAC will provide CommercioKYC Data as Currency will make use of the Overlays Data Capture Architecture (OCA) which will be developed mainly by a subgrantee called the Data Sharing Hub project. Data as Currency project builds a specified OCA layer carrying the value added to data. 

### SSISharing  (paul.knowles@humancolossus.org)
* CommercioKYC will provide SSISharing the DID Wallet and DIDauth infrastructure plus the SEPA payment KYC Credentials workflow 
* SSISharing will provide CommercioKYC   the Overlays Capture Architecture (OCA) repository and data vault components. Those integrations will be useful for allowing users (SSI wallet holders) to interact in its vision of a Dynamic Data Economy.

### NYM-CSSI  (harry@nymtech.net)
* CommercioKYC will provide to  NYM-CSSI  users on our KYC onboarding Commercio.app to the initial alpha launch of NYM-CSSI project. Users will be able to search in our app store to find CommercioKYC’s onboarding app.
* NYM-CSSI will provide  CommercioKYC  a privacy and security review of the KYC architecture NYM CCI will look into making sure we work with the KYC requirements of CommercioKYC using Nym Credentials and any other relevant standards.

### Verifiablecredentials.info (d.w.chadwick@kent.ac.uk)
* Verifiablecredentials.info will provide their Policy Management infrastructure to CommericioKYC
* CommercioKYC will provide its KYC ptotocol to Verifiablecredentials.info


### MDCommons (iainhenderson@mac.com)
* CommercioKYC will provide its eID SDK to MDCommons  to implement  the  specific  KYC Verifiable Credential use case for all consortium participants
* MDCommons will implement  CommercioKYC  specific use case for its KYC Verifiable Credential using their governed data set with the JLINC Protocol to demonstrate interoperability
